#include <stdio.h>
 
int main() {
    
    int i, num, proxtermo, t1 = 0, t2 = 1;
    
    scanf("%d", &num);
    
    for(i = 1; i <= num; i++){
        if(i != num){
        printf("%d ",t1);
        proxtermo = t1 + t2;
        t1 = t2;
        t2 = proxtermo;
        }else{
        printf("%d\n", t1);
        }
    }
    return 0;
}