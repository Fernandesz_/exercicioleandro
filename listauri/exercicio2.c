#include <stdio.h>

int main (){

    float hour, speed, litters;
    
    scanf("%f", &hour);
    scanf("%f", &speed);
    
    litters = (speed*hour) /12;
    
    printf("%.3f", litters);
    
    return 0;

}