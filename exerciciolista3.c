#include <stdio.h>

int main(){

    int num, fatorial;

    printf("Digite um valor para calcular o fatorial: ");
    scanf("%d", &num);

    for (fatorial = 1; num > 1; num = num - 1 ){
        fatorial = fatorial * num;
    }

    printf("\nO fatorial calculado foi: %d", fatorial);
    
}