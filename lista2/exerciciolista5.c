#include<stdio.h>
#include<string.h>

int main() {
    int valor = 0;
    char str[15], inverso[15];

    printf("\nDigite uma palavra: ");
    gets(str);
    
    strcpy(inverso, str);

    //Inverte a palavra copiada
    strrev(inverso);

    valor = strcmp(str, inverso);

    if (valor == 0){
        printf("A palavra %s e palindroma\n", str);
    }
    else{
        printf("A palavra %s nao e palindroma\n", str);
    }
    return 0;
}