#include <stdio.h>

int main(){
    int ano, resto;

    printf("Digite o ano numerico para saber se ele e bissexto: ");
    scanf("%d", &ano);

   resto = ano % 4;

   if (resto == 0)
       printf("O ano %d e bissexto", ano);
   else
       printf("O ano %d nao e bissexto", ano);
   
}