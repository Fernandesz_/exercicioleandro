#include <stdio.h>
#include <math.h>

int main()

{
    int init, razao, n_terms, termo_pa,
        count;

    printf("Elemento inicial da P.A.: \n");
    scanf("%d", &init);

    printf("Razao da P.A.: \n");
    scanf("%d", &razao);

    printf("Numero de termos da P.A.: \n");
    scanf("%d", &n_terms);

    for (count = 0; count <= n_terms; count++)
    {
        termo_pa = init + count * razao;
        printf("Elemento %d da P.A.: %d \n", count, termo_pa);
    }
}


  